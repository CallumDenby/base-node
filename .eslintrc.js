module.exports = {
    "extends": [
        "standard",
        "plugin:flowtype/recommended"
    ],
    "plugins": [
        "flowtype"
    ],
    "settings": {
        "flowtype": {
            "onlyFilesWithFlowAnnotation": true
        }
    }
};