FROM mhart/alpine-node:8
ARG build="true"

RUN mkdir /src

WORKDIR /src
ADD package.json .
ADD yarn.lock .
RUN yarn

ADD . .

RUN if [ "$build" = "true" ]; then yarn build; fi
CMD node dist/index.js